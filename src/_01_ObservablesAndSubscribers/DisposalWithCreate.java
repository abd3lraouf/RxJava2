package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DisposalWithCreate {
    public static void main(String[] args) throws InterruptedException {
        Observable<Integer> objectObservable = Observable.create(e -> {
            try {
                for (int i = 0; i < 1000; i++) {
//                    while (!e.isDisposed()) {
                        e.onNext(i);
                        if (e.isDisposed())
                            return;

                        Thread.sleep(1000);
//                    }
                }
                e.onComplete();
            } catch (Throwable err) {
                e.onError(err);
            }
        });


        Disposable subscribe = objectObservable
                .subscribeOn(Schedulers.computation())
                .subscribe(System.out::println, throwable -> {});
        System.out.println("Main sleep");
        Thread.sleep(5000);
        System.out.println("Main wake");
        subscribe.dispose();
        System.out.println("Disposed");
    }
}
