package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class IntervalHotOrCold {
    public static void main(String[] args) {
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS);

        seconds.subscribe(aLong -> System.out.println("O1: " + aLong));

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        seconds.subscribe(aLong -> System.out.println("O2: " + aLong));


        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("YaaY it's cold");

        System.out.println("Separate timers");
    }
}
