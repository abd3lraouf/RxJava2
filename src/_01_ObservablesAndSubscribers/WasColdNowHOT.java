package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;
import utils.OBSERVABLE;

public class WasColdNowHOT {
  public static void main(String[] args) {
    Observable<String> cold = OBSERVABLE.just;
    ConnectableObservable<String> hot = cold.publish();

    hot.subscribe(s -> System.out.println("O1: " + s));
    hot
            .map(String::length)
            .subscribe(s -> System.out.println("O2: " + s));

    hot.connect();

  }
}
