package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;

public class createWithError {
  public static void main(String[] args) {
    Observable<String> source = Observable.create(emitter -> {

      try {
        emitter.onNext("1");
        emitter.onNext("2");
        emitter.onNext("3");
        emitter.onNext("4");
        emitter.onNext("5");
        emitter.onNext("6");
        emitter.onNext("7");
        emitter.onComplete();
      } catch (Throwable throwable) {
        emitter.onError(throwable);
      }
    });

    source.subscribe(s -> System.out.println("Received: " + s), Throwable::printStackTrace);

  }
}
