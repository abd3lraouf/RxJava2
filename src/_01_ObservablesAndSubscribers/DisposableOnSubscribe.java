package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import utils.OBSERVABLE;

public class DisposableOnSubscribe {
    public static void main(String[] args) {

        Observer<String> observer = new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String s) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        Observable<String> just = OBSERVABLE.just;
        Disposable subscribe = just.subscribe(System.out::println);


    }
}
