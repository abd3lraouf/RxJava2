package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class AGirlForAnInterval {
    public static void main(String[] args) {
        Observable.interval(1, TimeUnit.SECONDS)
                .subscribe(s -> System.out.println(s + " Mississippi"));

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
