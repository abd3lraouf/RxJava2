package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;
import utils.OBSERVABLE;

public class ItsCold {
  public static void main(String[] args) {
    Observable<String> src = OBSERVABLE.just;

    src.subscribe(s -> System.out.println("O1: " + s));
    src.subscribe(s -> System.out.println("O2: " + s));
    System.out.println("It just repeats the sequence, like a Music CD");

  }
}
